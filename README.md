# Fake News

Fake News is a preset for the Moodle activity database.

## Getting started

Download the source code and zip the files WITHOUT parent folder. Create a "Database" activity in Moodle and then upload the ZIP file in the "Presets" tab under "Import".

## Language Support

The preset is available in German. 

## Description

Students can create fake news.

## License

CC BY-NC-SA 2.0 DE

## Screenshots

<img width="400" alt="single view" src="/screenshots/einzelansicht.png">

<img width="400" alt="list view" src="/screenshots/listenansicht.png">
